# Simple Meetings

This application was created to solve events problems, basicily that.

## Development instructions

It's easy to start the development code or just run the application, run the instructions in your console

- ``bundle install``
- ``yarn``
- ``rails db:drop db:create db:migrate db:seed``
- ``rails s``

Finally access http://localhost:3000

## Tests

Just run

- ``rspec .``

or if you whant test your code in real-time run

- ``bundle exec guard``
