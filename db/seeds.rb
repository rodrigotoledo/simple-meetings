require 'faker'

User.create(email: 'user@simple_meetings.com', password: 'asdqwe123', password_confirmation: 'asdqwe123')
User.create(email: 'rodrigo@rtoledo.inf.br', password: 'asdqwe123', password_confirmation: 'asdqwe123')
User.all.each do |user|
  2.times do
    room = Room.create(title: Faker::Job.title)
    date_start_month = Date.today.beginning_of_month
    date_end_month = Date.today.end_of_month
    days = (date_end_month - date_start_month).to_i
    days.times.each do |i|
      day = date_start_month.change(day: i+1)
      meeting = room.meetings.build(
        user_id: user.id,
        title: 'Daily Meeting',
        day: day,
        scheduled_at: Time.now.change(hour: 9)
      )
      meeting.save
    end
  end
end
