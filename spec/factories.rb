FactoryBot.define do
  factory :user do
    email { 'user@simple_meetings.com' }
    password { 'asdqwe123' }
    password_confirmation { 'asdqwe123' }
  end

  factory :room do
    title { Faker::Job.title }
  end

  factory :meeting do
    room factory: :room
    user factory: :user
    title { Faker::Company.industry }
    day { "2020-10-14".to_date }
    scheduled_at { Time.now.change(hour: 9) }
  end
end
