require 'rails_helper'

RSpec.describe Meeting, type: :model do
  describe "create and update actions" do
    before do
      User.destroy_all
    end
    context "positive scenarios" do
      it "easy to create" do
        meeting = Meeting.create(attributes_for(:meeting))
        expect(meeting).to be_truthy
      end
    end

    context "negative scenarios" do
      it "validate presence of title" do
        meeting = Meeting.new
        expect(meeting.valid?).to be_falsey
        expect(meeting.errors_on(:title)).to include("can't be blank")
        expect(meeting.errors_on(:day)).to include("can't be blank")
        expect(meeting.errors_on(:scheduled_at)).to include("can't be blank")
        expect(meeting.errors_on(:room)).to include("must exist")
        expect(meeting.errors_on(:user)).to include("must exist")
      end

      it "validate uniqueness title for user" do
        meeting = create(:meeting)
        user = create(:user, email: 'other@email.com')
        new_meeting = meeting.dup
        new_meeting.user_id = user.id
        expect(new_meeting.save).to be_falsey
        expect(new_meeting.errors_on(:title)).to include("has already been taken")
      end

      it "validate uniqueness meeting by user, room, meeting: title, day, and scheduled_at" do
        meeting = create(:meeting)
        another_user = create(:user, email: 'other@email.com')
        invalid_meeting = meeting.dup
        invalid_meeting.user_id = another_user.id
        expect(invalid_meeting.valid?).to be_falsey
        expect(invalid_meeting.errors_on(:title)).to include("has already been taken")
        invalid_meeting.day = invalid_meeting.day + 1.day
        expect(invalid_meeting.valid?).to be_truthy
        invalid_meeting.day = invalid_meeting.day - 1.day
        invalid_meeting.scheduled_at = invalid_meeting.scheduled_at + 1.hour
        expect(invalid_meeting.valid?).to be_truthy
      end

    end

  end
end
