# require 'rails_helper'

# RSpec.describe "Rooms", type: :request do

#   let!(:user){create(:user)}
#   before(:each) do
#     login_as(user)
#   end
#   describe "GET /index" do
#     before do
#       5.times do |i|
#         user.rooms.build(title: "Daily Meeting #{i}")
#         user.save
#       end
#     end

#     it "returns http success with correct data" do
#       get rooms_path
#       expect(response).to have_http_status(:success)
#       expect(assigns(:rooms)).to have(5).items
#     end
#   end

#   describe "POST /create" do
#     it "Create with success" do
#       expect { post rooms_path, params: {room: attributes_for(:room) } }.to change(Room, :count).by(1)
#     end
#   end

#   describe "DESTROY /destroy" do
#     before do
#       Room.destroy_all
#     end
#     it "Delete with success" do
#       room = create(:room, user: user)
#       delete room_path(room.id)
#       expect(response).to have_http_status(302)
#       expect(Room.count).to equal(0)
#     end
#   end

# end
