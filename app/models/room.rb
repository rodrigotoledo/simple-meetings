class Room < ApplicationRecord
  default_scope { order(created_at: :desc) }
  has_many :meetings, autosave: true, dependent: :destroy

  validates :title, presence: true
end
