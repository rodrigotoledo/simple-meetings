class Meeting < ApplicationRecord
  default_scope { order(scheduled_at: :asc) }
  belongs_to :room
  belongs_to :user
  validates :title, :day, :scheduled_at, presence: true
  validates :title, uniqueness: true, if: :uniqueness_by_user?
  validates_associated :room
  validates_associated :user

  attr_accessor :date_start, :date_end

  def start_time
    day
  end

  protected

    def identical_meeting
      Room.joins(:meetings).where("
        rooms.title = ? AND
        meetings.user_id != ? AND
        meetings.day = ? AND
        meetings.scheduled_at = ?",
          room.title,
          user.id,
          day,
          scheduled_at
      ).exists?
    end

    def uniqueness_by_user?
      if room.present? && user.present? && title.present? && day.present? && scheduled_at.present? && !identical_meeting
        false
      else
        true
      end
    end
end
